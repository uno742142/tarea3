import { DataSource } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { ActualizarProductoDto, CrearProductoDto } from '../dto'
import { Producto } from '../entity'
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'

@Injectable()
export class ProductoRepository {
  constructor(private dataSource: DataSource) {}

  async crear(productoDto: CrearProductoDto, usuarioAuditoria: string) {
    const { codigo, nombre, precio } = productoDto

    const producto = new Producto()
    producto.codigo = codigo
    producto.nombre = nombre
    producto.precio = precio
    producto.usuarioCreacion = usuarioAuditoria

    return await this.dataSource.getRepository(Producto).save(producto)
  }

  async listar(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar } = paginacionQueryDto
    const query = this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .select([
        'producto.id',
        'producto.codigo',
        'producto.nombre',
        'producto.precio',
        'producto.estado',
      ])
      .take(limite)
      .skip(saltar)

    return await query.getManyAndCount()
  }

  async buscarCodigo(codigo: string) {
    return this.dataSource
      .getRepository(Producto)
      .findOne({ where: { codigo: codigo } })
  }

  async buscarPorId(id: string) {
    return await this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .where({ id: id })
      .getOne()
  }

  async actualizar(
    id: string,
    productoDto: ActualizarProductoDto,
    usuarioAuditoria: string
  ) {
    const { codigo, nombre, precio } = productoDto
    const datosActualizar: QueryDeepPartialEntity<Producto> = new Producto({
      codigo: codigo,
      nombre: nombre,
      precio: precio,
      usuarioModificacion: usuarioAuditoria,
    })
    return await this.dataSource
      .getRepository(Producto)
      .update(id, datosActualizar)
  }

  async eliminarProductoRepository(id:string){
    const response=await this.dataSource.getRepository(Producto).delete(id);
    return response;
  }
}
