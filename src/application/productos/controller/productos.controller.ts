import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common'
import { ProductosService } from '../service'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { BaseController } from '../../../common/base'
import { ParamIdDto } from '../../../common/dto/params-id.dto'
import { Request } from 'express'
import { ActualizarProductoDto, CrearProductoDto } from '../dto'
import { JwtAuthGuard } from '../../../core/authentication/guards/jwt-auth.guard'
import { CasbinGuard } from '../../../core/authorization/guards/casbin.guard'

@Controller('productos')
//@UseGuards(JwtAuthGuard, CasbinGuard)
export class ProductosController extends BaseController {
  constructor(private productosServicio: ProductosService) {
    super()
  }

  @Get()
  async listar(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.productosServicio.listar(paginacionQueryDto)
    return this.successListRows(result)
  }

  @Post()
  async crear(@Req() req: Request, @Body() productoDto: CrearProductoDto) {
    // const usuarioAuditoria = this.getUser(req)
    const usuarioAuditoria = '1'
    const result = await this.productosServicio.crear(
      productoDto,
      usuarioAuditoria
    )
    return this.successCreate(result)
  }

  @Put(':id')
  async actualizar(
    @Param() params: ParamIdDto,
    @Req() req: Request,
    @Body() productoDto: ActualizarProductoDto
  ) {
    const { id: idProducto } = params
    // const usuarioAuditoria = this.getUser(req)
    const usuarioAuditoria = '1'
    const result = await this.productosServicio.actualizarDatos(
      idProducto,
      productoDto,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }

  @Delete('/:id')
  async eliminarProducto(@Param() paran: ParamIdDto) {
    const response = await this.productosServicio.eliminarProductoServicio(paran.id)
    return response;
  }
}
